import path from 'path';
import fs from 'fs-extra';
import babel from "../common/babel";
import { config } from './const';
import { buildSinglePage } from '../common/buildSinglePage';

const outputDir = path.resolve(__dirname, '../../dist');
const inputRoot = path.join(path.resolve('.'), 'src');

async function init() {
  fs.removeSync(outputDir)
  fs.ensureDirSync(outputDir)
}

async function buildProjectConfig() {
  fs.writeFileSync(
    path.join(outputDir, 'project.config.json'),
    `{
    "miniprogramRoot": "./",
    "projectname": "app",
    "description": "app",
    "appid": "touristappid",
    "setting": {
        "urlCheck": true,
        "es6": false,
        "postcss": false,
        "minified": false
    },
    "compileType": "miniprogram"
}
`
  );
}

/**
 *  输出入口文件app.js与app.json
 */
async function buildEntry() {
  fs.writeFileSync(path.join(outputDir, "./app.js"), `App({})`);
  const config = require(path.resolve(inputRoot, "app.config.js"));
  fs.writeJsonSync(
    path.join(outputDir, "./app.json"),
    config
  );
}

/**
 * npm拷贝到输出目录
 */
async function copyNpmToWx() {
  const npmPath = path.resolve(__dirname, './npm')
  const allFiles = await fs.readdirSync(npmPath)
  allFiles.forEach(async (fileName) => {
    const fileContent = fs.readFileSync(path.join(npmPath, fileName)).toString()
    const outputNpmPath = path.join(outputDir, `./npm/${fileName}`)
    let resCode = await babel(fileContent, outputNpmPath)
    fs.ensureDirSync(path.dirname(outputNpmPath))
    fs.writeFileSync(outputNpmPath, resCode.code)
  })
}

async function buildPages() {
  config.pages.forEach((page) => {
    buildSinglePage(page);
  });
}

async function main() {
  // 检查目录等准备工作
  await init();
  // npm拷贝到输出目录
  await copyNpmToWx();
  // 页面生成4种输出到输出目录
  await buildPages();
  // 输出入口文件app.js与app.json
  await buildEntry();
  // 输出project.config.json
  await buildProjectConfig();
}
main();
