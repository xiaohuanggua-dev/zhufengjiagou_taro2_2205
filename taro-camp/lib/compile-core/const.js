"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.outputDir = exports.inputRoot = exports.config = void 0;

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const outputDir = _path.default.resolve(__dirname, "../../dist");

exports.outputDir = outputDir;

const inputRoot = _path.default.join(_path.default.resolve("."), "src");

exports.inputRoot = inputRoot;

const config = require(_path.default.resolve(inputRoot, "app.config.js"));

exports.config = config;