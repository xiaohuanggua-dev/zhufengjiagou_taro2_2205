"use strict";

var _path = _interopRequireDefault(require("path"));

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _babel = _interopRequireDefault(require("../common/babel"));

var _const = require("./const");

var _buildSinglePage = require("../common/buildSinglePage");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const outputDir = _path.default.resolve(__dirname, '../../dist');

const inputRoot = _path.default.join(_path.default.resolve('.'), 'src');

async function init() {
  _fsExtra.default.removeSync(outputDir);

  _fsExtra.default.ensureDirSync(outputDir);
}

async function buildProjectConfig() {
  _fsExtra.default.writeFileSync(_path.default.join(outputDir, 'project.config.json'), `{
    "miniprogramRoot": "./",
    "projectname": "app",
    "description": "app",
    "appid": "touristappid",
    "setting": {
        "urlCheck": true,
        "es6": false,
        "postcss": false,
        "minified": false
    },
    "compileType": "miniprogram"
}
`);
}
/**
 *  输出入口文件app.js与app.json
 */


async function buildEntry() {
  _fsExtra.default.writeFileSync(_path.default.join(outputDir, "./app.js"), `App({})`);

  const config = require(_path.default.resolve(inputRoot, "app.config.js"));

  _fsExtra.default.writeJsonSync(_path.default.join(outputDir, "./app.json"), config);
}
/**
 * npm拷贝到输出目录
 */


async function copyNpmToWx() {
  const npmPath = _path.default.resolve(__dirname, './npm');

  const allFiles = await _fsExtra.default.readdirSync(npmPath);
  allFiles.forEach(async fileName => {
    const fileContent = _fsExtra.default.readFileSync(_path.default.join(npmPath, fileName)).toString();

    const outputNpmPath = _path.default.join(outputDir, `./npm/${fileName}`);

    let resCode = await (0, _babel.default)(fileContent, outputNpmPath);

    _fsExtra.default.ensureDirSync(_path.default.dirname(outputNpmPath));

    _fsExtra.default.writeFileSync(outputNpmPath, resCode.code);
  });
}

async function buildPages() {
  _const.config.pages.forEach(page => {
    (0, _buildSinglePage.buildSinglePage)(page);
  });
}

async function main() {
  // 检查目录等准备工作
  await init(); // npm拷贝到输出目录

  await copyNpmToWx(); // 页面生成4种输出到输出目录

  await buildPages(); // 输出入口文件app.js与app.json

  await buildEntry(); // 输出project.config.json

  await buildProjectConfig();
}

main();