"use strict";

var _path = _interopRequireDefault(require("path"));

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _babel = _interopRequireDefault(require("../common/babel"));

var _buildSinglePage = require("../common/buildSinglePage");

var _const = require("./const");

var _transform = _interopRequireDefault(require("./transform"));

var _base = require("./base");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * npm拷贝到输出目录
 */
async function copyNpmToWx() {
  const npmPath = _path.default.resolve(__dirname, "./npm");

  const allFiles = await _fsExtra.default.readdirSync(npmPath);
  allFiles.forEach(async fileName => {
    const fileContent = _fsExtra.default.readFileSync(_path.default.join(npmPath, fileName)).toString();

    const outputNpmPath = _path.default.join(_const.outputDir, `./npm/${fileName}`);

    let resCode = await (0, _babel.default)(fileContent, outputNpmPath);

    _fsExtra.default.ensureDirSync(_path.default.dirname(outputNpmPath));

    _fsExtra.default.writeFileSync(outputNpmPath, resCode.code);
  });
}
/**
 *  页面生成4种输出到输出目录
 */


async function buildPages() {
  _const.config.pages.forEach(page => {
    (0, _buildSinglePage.buildSinglePage)(page, _transform.default);
  });
}
/**
 *  输出入口文件app.js与app.json
 */


async function buildEntry() {
  _fsExtra.default.writeFileSync(_path.default.join(_const.outputDir, "./app.js"), `App({})`);

  _fsExtra.default.writeFileSync(_path.default.join(_const.outputDir, "./app.json"), JSON.stringify(_const.config, undefined, 2));
}
/**
 *  输出project.config.json
 */


async function buildProjectConfig() {
  _fsExtra.default.writeFileSync(_path.default.join(_const.outputDir, "project.config.json"), `
{
    "miniprogramRoot": "./",
    "projectname": "app",
    "description": "app",
    "appid": "touristappid",
    "setting": {
        "urlCheck": true,
        "es6": false,
        "postcss": false,
        "minified": false
    },
    "compileType": "miniprogram"
}
    `);
}
/**
 *  检查目录等准备工作
 */


async function init() {
  _fsExtra.default.removeSync(_const.outputDir);

  _fsExtra.default.ensureDirSync(_const.outputDir);
}

async function geneBaseWxml() {
  const outputBaseWxml = _path.default.join(_const.outputDir, "/base.wxml");

  _fsExtra.default.ensureDirSync(_path.default.dirname(outputBaseWxml));

  _fsExtra.default.writeFileSync(outputBaseWxml, _base.baseWxml);
}

async function main() {
  // 检查目录等准备工作
  await init(); // npm拷贝到输出目录

  await copyNpmToWx(); // 拷贝base到根目录

  await geneBaseWxml(); // 页面生成4种输出到输出目录

  await buildPages(); // 输出入口文件app.js与app.json

  await buildEntry(); // 输出project.config.json

  await buildProjectConfig();
}

main();